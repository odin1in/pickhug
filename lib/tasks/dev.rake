namespace :dev do
  task fake: %i[db:reset environment] do
    user = User.create!(email: "odin@pwn.so", password: "test123", name: "odin")
    access_token = user.accesstokens.create!

    puts access_token.uuid

    articles = ['覺人無親春源表大特！辦何親，他人北，讓解人！作到我亮們，道了型有選，由同決來。情臺利，中晚新錯庭月是，不高過裝失死。們而員合十！玩民如放有形那走活十臺訴光論我問看天樣轉地研清媽，主自舉件動，空多小差的工來別教不得生自未直興完重熱越關面期？事新拉與：了位們就收比。坡來黃經也不金再導爭也真它！其臺過現民水力通體一是兩家一放麼老告的。',
                '書農到首；上利去些道你。',
                '媽感車度地邊外他。物公息員書區地、各香層影的在兩黃色中子，務留行驗光在會心氣視裡，要有想超如較清有往使大臺字軍，了出心得團不否境此！']

    comments = ['媽感車度地邊外他。物公息員書區地、各香層影的在兩黃色中子，務留行驗光在會心氣視裡，要有想超如較清有往使大臺字軍，了出心得團不否',
                '媽感車度地邊外他。',
                '媽感車度地邊外他。媽感車度地邊外他。媽感車度地邊外他。媽感車度地邊外他。媽感車度地邊外他。媽感車度地邊外他。媽感車度地邊外他。媽感車度地邊外他。媽感車度地邊外他。媽感車度地邊外他。媽感車度地邊外他。']
    articles.each do |article|
      article = user.articles.create!(content: article)

      comments.sample(rand(3)).each do |comment|
        article.comments.create!(user: user, content: comment)
      end
    end
  end
end
