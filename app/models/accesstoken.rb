class Accesstoken < ActiveRecord::Base
  # Scope macros

  # Concerns macros

  # Constants

  # Attributes related macros

  # Association macros
  belongs_to :user

  # Validation macros
  validates :user, presence: true

  # Callbacks
  before_create :set_uuid
  
  # other

  private
    def set_uuid
      self.uuid = SecureRandom.uuid
    end
end
