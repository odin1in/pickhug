class Report < ActiveRecord::Base
  # Scope macros
  enum status: %i( processing done reject)

  # Concerns macros
  mount_uploader :pic1, ReportPicUploader
  mount_uploader :pic2, ReportPicUploader
  mount_uploader :pic3, ReportPicUploader
  # Constants

  # Attributes related macros

  # Association macros
  belongs_to :user
  
  # Validation macros
  validates :note, presence: true

  # Callbacks

  # other
end
