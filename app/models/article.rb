class Article < ActiveRecord::Base
  # Scope macros
  scope :by_newest, -> { order(created_at: :desc) }
  
  # Concerns macros

  # Constants

  # Attributes related macros

  # Association macros
  belongs_to :user
  has_many :knows, dependent: :destroy
  has_many :comments, dependent: :destroy
  
  # Validation macros
  validates :content, presence: true

  # Callbacks

  # other

end
