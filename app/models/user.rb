class User < ActiveRecord::Base
  # Scope macros

  # Concerns macros
  has_secure_password

  # Constants

  # Attributes related macros

  # Association macros
  has_many :accesstokens
  has_many :articles
  has_many :reports
  
  # Validation macros
  validates :email, :name, presence: true
  validates :email, uniqueness: true

  # Callbacks

  # other

  def to_s
    name.to_s
  end

  def identity
    read_attribute(:identity) || ""
  end

  def phone
    read_attribute(:phone) || ""
  end

  def name
    read_attribute(:name) || ""
  end

  def address
    read_attribute(:address) || ""
  end
end
