class Know < ActiveRecord::Base

  # Scope macros

  # Concerns macros

  # Constants

  # Attributes related macros

  # Association macros
  belongs_to :article, counter_cache: true
  belongs_to :user
  
  # Validation macros
  validates :article, presence: true, uniqueness: { scope: :user }

  # Callbacks

  # other

  


end
