json.array!(@articles) do |article|
  json.extract! article, :id, :content, :knows_count
  json.created_at I18n.l(article.created_at, format: :long)
  json.updated_at I18n.l(article.updated_at, format: :long)
  json.user do
    json.id     article.user.id
    json.name   article.user.name
  end
  json.comments article.comments do |comment|
    json.extract! comment, :id, :content, :created_at, :updated_at
    json.user do
      json.id     comment.user.id
      json.name   comment.user.name
    end
  end
end
