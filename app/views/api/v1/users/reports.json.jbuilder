json.array!(@reports) do |report|
  json.extract! report, :id, :note, :location, :updated_at, :created_at, :status
  json.status I18n.t("report_status.#{report.status}")
  json.violate_at I18n.l(report.violate_at, format: :long)
  json.pic1 report.pic1? ? "https://repics.storage.googleapis.com/#{report.pic1.path}" : ""
  json.pic2 report.pic2? ? "https://repics.storage.googleapis.com/#{report.pic2.path}" : ""
  json.pic3 report.pic3? ? "https://repics.storage.googleapis.com/#{report.pic3.path}" : ""
  json.user do
    json.id     report.user.id
    json.name   report.user.name
  end
end
