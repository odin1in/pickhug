json.extract! @article, :id, :content, :knows_count, :created_at, :updated_at
json.user do
  json.id     @article.user.id
  json.name   @article.user.name
end

json.comments @article.comments do |comment|
  json.extract! comment, :id, :content, :created_at, :updated_at
  json.user do
    json.id     comment.user.id
    json.name   comment.user.name
  end
end
