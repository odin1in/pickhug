json.extract! @report, :id, :note, :violate_at, :location
json.pic1 "https://repics.storage.googleapis.com/#{@report.pic1.path}"
json.pic2 "https://repics.storage.googleapis.com/#{@report.pic2.path}"
json.pic3 "https://repics.storage.googleapis.com/#{@report.pic3.path}"
json.user do
  json.id     @report.user.id
  json.name   @report.user.name
end
