class ApplicationMailer < ActionMailer::Base
  default from: "support@pickhug.com"
  layout 'mailer'
end
