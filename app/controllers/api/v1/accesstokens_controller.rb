class Api::V1::AccesstokensController < Api::V1::BaseController
  def create
    user = User.find_by_email(params[:email])

    if user && user.authenticate(params[:password])
      @accesstoken = Accesstoken.create!(user: user)
      render :show, status: :created
    else
      render json: { password: ["帳號或密碼錯誤"]}, status: :unprocessable_entity
    end
  end
end
