class Api::V1::ReportsController < Api::V1::BaseController
  before_action :authenticate!

  def create
    @report = current_user.reports.new(report_params)
    @report.violate_at -= 8.hours if @report.violate_at.present?
    
    if @report.save
      render :show, status: :created
    else
      render json: @report.errors, status: :unprocessable_entity
    end
  end

  private
    def report_params
      params.require(:report).permit(:note, :violate_at, :location, :category, :category_other, :pic1, :pic2, :pic3)
    end
end
