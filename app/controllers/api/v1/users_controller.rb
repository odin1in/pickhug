class Api::V1::UsersController < Api::V1::BaseController
  before_action :authenticate!, only: [:update, :reports, :articles, :show]

  def create
    @user = User.new(user_params)

    if @user.save
      render :show, status: :created
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def show
    @user = current_user
  end

  def update
    @user = User.find(current_user.id)

    if @user.update(user_params)
      render :show, status: :ok
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def forgot
    user = User.find_by_email(@email = params[:email])
    if user 
      new_password = SecureRandom.hex(4)
      user.update_attribute(:password, new_password)
      UserMailer.forgot(user.email, new_password).deliver
    end
  end

  def reports
    @reports = current_user.reports
  end

  def articles
    @articles = current_user.articles
  end

  private
    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation, :name, :address, :phone, :identity)
    end

    def update_user_params
      user_params.except(:email)
    end
end
