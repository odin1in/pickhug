class Api::V1::BaseController < ApiController

  def authenticate!
    @access_token = Accesstoken.includes(:user).find_by_uuid(params[:accesstoken])
    head :forbidden unless current_user
  end

  def current_user
    @current_user ||= @access_token.try(:user)
  end
end
