class Api::V1::ArticlesController < Api::V1::BaseController
  before_action :authenticate!
  before_action :set_article, only: [:know, :comments]

  def index
    @articles = Article.by_newest
  end

  def create
    @article = current_user.articles.new(article_params)

    if @article.save
      render :show, status: :created
    else
      render json: @article.errors, status: :unprocessable_entity
    end
  end

  def know
    @article.knows.create(user: current_user)
    render :show, status: :created
  end

  def comments
    comment = @article.comments.new(comment_params)
    comment.user = current_user

    if comment.save
      render :show, status: :created
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  private
    def set_article
      @article = Article.find(params[:article_id])
    end

    def article_params
      params.require(:article).permit(:content)
    end

    def comment_params
      params.require(:comment).permit(:content)
    end
end
