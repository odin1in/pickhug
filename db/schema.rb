# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151213185905) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accesstokens", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "uuid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "accesstokens", ["user_id"], name: "index_accesstokens_on_user_id", using: :btree
  add_index "accesstokens", ["uuid"], name: "index_accesstokens_on_uuid", using: :btree

  create_table "articles", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "content"
    t.integer  "knows_count", default: 0, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "region"
  end

  add_index "articles", ["region"], name: "index_articles_on_region", using: :btree
  add_index "articles", ["user_id"], name: "index_articles_on_user_id", using: :btree

  create_table "comments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "article_id"
    t.string   "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "comments", ["article_id"], name: "index_comments_on_article_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "knows", force: :cascade do |t|
    t.integer  "article_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "knows", ["article_id"], name: "index_knows_on_article_id", using: :btree
  add_index "knows", ["user_id"], name: "index_knows_on_user_id", using: :btree

  create_table "reports", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "note"
    t.string   "location"
    t.datetime "violate_at"
    t.integer  "status",         default: 0, null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "region"
    t.string   "category"
    t.string   "category_other"
    t.string   "pic1"
    t.string   "pic2"
    t.string   "pic3"
  end

  add_index "reports", ["category"], name: "index_reports_on_category", using: :btree
  add_index "reports", ["category_other"], name: "index_reports_on_category_other", using: :btree
  add_index "reports", ["region"], name: "index_reports_on_region", using: :btree
  add_index "reports", ["user_id"], name: "index_reports_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.string   "password_digest"
    t.string   "name"
    t.string   "address"
    t.string   "phone"
    t.string   "identity"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_foreign_key "accesstokens", "users"
  add_foreign_key "articles", "users"
  add_foreign_key "comments", "articles"
  add_foreign_key "comments", "users"
  add_foreign_key "knows", "articles"
  add_foreign_key "knows", "users"
  add_foreign_key "reports", "users"
end
