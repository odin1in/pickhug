class AddCategoryOthersToReport < ActiveRecord::Migration
  def change
    add_column :reports, :category_other, :string
    add_index :reports, :category_other
  end
end
