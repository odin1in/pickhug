class CreateAccesstokens < ActiveRecord::Migration
  def change
    create_table :accesstokens do |t|
      t.references :user, index: true, foreign_key: true
      t.string :uuid, index: true, unique: true

      t.timestamps null: false
    end
  end
end
