class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.references :user, index: true, foreign_key: true
      t.string :content
      t.integer :article_knows_count, null: false, default: 0

      t.timestamps null: false
    end
  end
end
