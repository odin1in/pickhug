class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.references :user, index: true, foreign_key: true
      t.text :note
      t.string :location
      t.datetime :violate_at
      t.integer :status, null: false, default: 0

      t.timestamps null: false
    end
  end
end
