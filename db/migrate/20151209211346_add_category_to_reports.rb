class AddCategoryToReports < ActiveRecord::Migration
  def change
    add_column :reports, :category, :string
    add_index :reports, :category
  end
end
