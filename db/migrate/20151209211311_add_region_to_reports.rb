class AddRegionToReports < ActiveRecord::Migration
  def change
    add_column :reports, :region, :string
    add_index :reports, :region
  end
end
