class AddRegionToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :region, :string
    add_index :articles, :region
  end
end
