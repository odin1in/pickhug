class RenameArticleKnowCountToArticleKnow < ActiveRecord::Migration
  def change
    rename_column :articles, :article_knows_count, :knows_count
  end
end
